import PropTypes from "prop-types";
import { useState, useEffect } from "react";
import "./MovieItem.css";
import { useLocalStorage } from "./useLocalStorage";

function MovieItem(props) {
  const { title, released, director, poster } = props;

  // TODO create a "favorite" state, default value : false




// Une façon de faire : 
// const [favorite, setFavorite] = useState(() => {
//   // garder les valeurs dans le local storage
//   const saved = localStorage.getItem("favorite");// récupérer la valeurs du localStorage
//   const initialValue = JSON.parse(saved);// on stocke le JSON récupéré et converti
//   return initialValue || false; // on dit au State de renvoyer soit ce qui est stocké dans la localstorage et quia été récupéré avant ou bien d'afficher la valeur par défaut qui est "false"( quand y'a rien dans le localstorage )
// });
// useEffect(() => {
//   // stocke favorite dans le localStorage
//   localStorage.setItem("favorite", JSON.stringify(favorite));
// }, [favorite]);


// Une autre façon de faire :
const [favorite, setFavorite] = useLocalStorage("favorite", false);


  // TODO add an action when clicking on the favorite button

function favoriteToggle(){
   setFavorite(!favorite) // passer l'inverse de la valeur de favorite au state
}






  return (
    <div className="MovieItem">
      <h2>{title}</h2>
      <img src={poster} alt={title} />
      <h4>Director: {director}</h4>
      <h5>Released: {released}</h5>

      {/*Rajout d'une action onClick avec favoriteToggle qui change la valeur de favorite, 
      et d'une condition ternaire pour afficher le status de la valeur actuelle du state */}
      <button type="button" onClick={favoriteToggle}>{favorite ? "Supprimer des favoris" : "Ajouter aux favoris"}</button> 
    </div>
  );
}

MovieItem.propTypes = {
  title: PropTypes.string.isRequired,
  released: PropTypes.string.isRequired,
  director: PropTypes.string.isRequired,
  poster: PropTypes.string.isRequired,
};

export default MovieItem;
